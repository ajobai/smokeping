# Smokeping

### Prerequisite 

* Install docker
* Install docker-compose 

### Download

`git clone https://gitlab.com/ajobai/smokeping.git`

### Configuration 

* Change the docker-compose.yml file to match the download path
* Update the config file Probes and Targets to desired settings 
* The default port used is 89, this can be changed if required in the docker-compose.yml

### Starting the project

`cd somkeping`
`docker-compose up -d`

Access the UI using the http://ip_address:89. 

<small>Have questions? Ask - anto.jobai@gmail.com<small>
